<?php
/**
 * Created by PhpStorm.
 * User: doanhcn2
 * Date: 14/03/2019
 * Time: 14:38
 */

namespace Magenest\Knockout\Controller\Ajax;


use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\Action;

class CheckEmail extends Action
{
    protected $feedbackFactory;

    public function __construct(Context $context, \Magenest\Knockout\Model\FeedbackFactory $feedbackFactory)
    {
        $this->feedbackFactory = $feedbackFactory;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $response = $this->resultFactory
            ->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
            ->setData([
                'status'  => false,
                'message' => "Email have already exists."
            ]);
        try {
            $email = $this->getRequest()->getParam('email');
            $feedbackModel = $this->feedbackFactory->create();
            $record = $feedbackModel->getCollection()->addFieldToFilter('email', $email);
            if (!count($record)){
                $response = $this->resultFactory
                    ->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
                    ->setData([
                        'status'  => true,
                        'message' => 'Email valid'
                    ]);
            }

        } catch (\Exception $exception){
            $response = $this->resultFactory
                ->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
                ->setData([
                    'status'  => false,
                    'message' => $exception->getMessage()
                ]);
        } finally {
            return $response;
        }
    }
}