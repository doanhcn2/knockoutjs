<?php
/**
 * Created by PhpStorm.
 * User: doanhcn2
 * Date: 14/03/2019
 * Time: 11:21
 */

namespace Magenest\Knockout\Controller\Ajax;


use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Setup\Exception;

class Submit extends Action
{
    protected $feedbackFactory;

    public function __construct(Context $context, \Magenest\Knockout\Model\FeedbackFactory $feedbackFactory)
    {
        $this->feedbackFactory = $feedbackFactory;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $response = $this->resultFactory
            ->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
            ->setData([
                'status'  => true,
                'message' => "form submitted correctly"
            ]);
        try {
            $formDataRequest = $this->getRequest()->getParams();
            $feedbackModel = $this->feedbackFactory->create();
            $feedbackModel->setData($formDataRequest);
            $feedbackModel->save();

        } catch (\Exception $exception){
            $response = $this->resultFactory
                ->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
                ->setData([
                    'status'  => false,
                    'message' => $exception
                ]);
        } finally {
            return $response;
        }
    }
}