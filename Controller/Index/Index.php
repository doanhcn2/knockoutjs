<?php
/**
 * Created by PhpStorm.
 * User: doanhcn2
 * Date: 12/03/2019
 * Time: 15:36
 */

// File: /app/code/Magenest/Knockout/Controller/Index/Index.php

namespace Magenest\Knockout\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;

class Index extends Action
{
    protected $_pageFactory;

    public function __construct(Context $context,\Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        // TODO: Implement execute() method.
        return $this->_pageFactory->create();
    }

}