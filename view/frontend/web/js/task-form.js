/*global define*/
define([
    'uiElement','ko'
], function(Element, ko) {
    'use strict';
    function Task(data) {
        this.title = ko.observable(data.title);
        this.isDone = ko.observable(data.isDone);
    }
    return Element.extend({
        defaults: {
            'template': 'Magenest_Knockout/task-template'
        },

        tasks: ko.observableArray([]),
        newTaskText: ko.observable(),
        incompleteTasks: ko.computed(function() {
            // return ko.utils.arrayFilter(
            //     tasks(),
            //     function(task) {
            //         return !task.isDone()
            //     });
        }),
        addTask: function() {
            this.tasks.push(new Task({
                    title: this.newTaskText()
                }));
            this.newTaskText("");
        },
        removeTask: function(task) { tasks.remove(task) }
    });
});