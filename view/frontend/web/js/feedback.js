/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define([
        "jquery",
        'ko',
        'uiComponent',
        'Magento_Ui/js/modal/alert',
        "jquery/ui",
        "mage/translate",
        "mage/mage",
        "mage/validation"
    ], function ($, ko, Component, alert, mage) {
        "use strict";

        return Component.extend({
            defaults: {
                msgSaved: false,
                template: 'Magenest_Knockout/feedback-form',
            },
            /** Initialize observable properties */
            initObservable: function () {
                this._super()
                    .observe('msgSaved')
                ;
                this.uname = ko.observable('');
                this.email = ko.observable('');
                this.phone = ko.observable('');
                this.msg = ko.observable('');
                this.isValidEmail = ko.observable(false);
                this.emailErrorMessage = ko.observable('');
                return this;
            },
            initialize: function (config) {
                this._super();
                return this;
            },

            validateEmail: function(){
                $.ajax({
                    url: this.getCheckEmailUrl,
                    data: {'email': this.email()},
                    type: 'post',
                    dataType: 'json',
                    context: this,
                    beforeSend: this._ajaxBeforeSend,
                    success: function (response) {
                        if (response.status == true){
                            this.isValidEmail(true);
                            this.emailErrorMessage('');
                        } else {
                            this.isValidEmail(false);
                            this.emailErrorMessage(response.message);
                        }
                    },
                    complete: this._ajaxComplete
                });
            },
            /**
             * Validate feedback form
             */
            validateForm: function () {
                var form = '#feedback-form';
                return $(form).validation() && $(form).validation('isValid');
            },
            submitFeedback: function () {

                if (!this.validateForm()) {
                    return;
                }
                var data = {'name':this.uname(),'email':this.email(),'phone':this.phone(),'message':this.msg(),'status':0};

                $.ajax({
                    url: this.getFeedbackUrl,
                    data: data,
                    type: 'post',
                    dataType: 'json',
                    context: this,
                    beforeSend: this._ajaxBeforeSend,
                    success: function (response) {
                        if (response.status === true){
                            this.msgSaved(true);
                            alert({
                                content: $.mage.__('Thanks for Submitting.')
                            });
                        }
                    },
                    complete: this._ajaxComplete
                });
            }
        });
    }
);