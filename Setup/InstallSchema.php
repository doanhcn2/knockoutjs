<?php
/**
 * Created by PhpStorm.
 * User: doanhcn2
 * Date: 14/03/2019
 * Time: 10:37
 */

namespace Magenest\Knockout\Setup;


use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        /**
         * Create table 'feedback'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('magenest_feedback'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'feedback id'
            )
            ->addColumn(
                'name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => false],
                'User name'
            )
            ->addColumn(
                'email',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                55,
                ['nullable' => false],
                'User email'
            )
            ->addColumn(
                'phone',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['nullable' => true],
                'User phone'
            )
            ->addColumn(
                'message',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => FALSE],
                'User message'
            )
            ->addColumn(
                'crated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                90,
                ['nullable' => FALSE],
                'created time'
            )
            ->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                3,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'feedback status'
            )
            ->addColumn(
                'approved_on',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                90,
                ['nullable' => FALSE],
                'approved time'
            )
            ->setComment('customer feedback details');
        $installer->getConnection()->createTable($table);
        $setup->endSetup();
    }
}