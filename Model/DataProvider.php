<?php
/**
 * Created by PhpStorm.
 * User: doanhcn2
 * Date: 13/03/2019
 * Time: 14:53
 */

namespace Magenest\Knockout\Model;


use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{
    public function __construct(
        $name, $primaryFieldName, $requestFieldName,
        \Magenest\Knockout\Model\ResourceModel\Feedback\CollectionFactory $collectionFactory,
        array $meta = [], array $data = [])
    {
        $this->collection = $collectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }
}