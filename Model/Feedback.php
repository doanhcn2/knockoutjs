<?php
/**
 * Created by PhpStorm.
 * User: doanhcn2
 * Date: 14/03/2019
 * Time: 10:40
 */

namespace Magenest\Knockout\Model;


use Magento\Framework\Model\AbstractModel;

class Feedback extends AbstractModel
{
    public function _construct()
    {
        $this->_init('Magenest\Knockout\Model\ResourceModel\Feedback');
    }
}