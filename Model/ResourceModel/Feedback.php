<?php
/**
 * Created by PhpStorm.
 * User: doanhcn2
 * Date: 14/03/2019
 * Time: 10:42
 */

namespace Magenest\Knockout\Model\ResourceModel;


class Feedback extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        // TODO: Implement _construct() method.
        $this->_init('magenest_feedback', 'entity_id');
    }
}