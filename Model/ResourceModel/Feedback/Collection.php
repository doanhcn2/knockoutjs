<?php
/**
 * Created by PhpStorm.
 * User: doanhcn2
 * Date: 14/03/2019
 * Time: 10:43
 */

namespace Magenest\Knockout\Model\ResourceModel\Feedback;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';
    /**
     *  Initialize resource collection
     *
     *  @return void
     */
    public function _construct()
    {
        $this->_init(\Magenest\Knockout\Model\Feedback::class, \Magenest\Knockout\Model\ResourceModel\Feedback::class);
    }
}